<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<c:set var="path" value="${pageContext.request.contextPath}"/>
<!DOCTYPE HTML>
<html>
<jsp:include page="../../common/head.jsp"></jsp:include>
<title>任务管理</title>
</head>
<body>
<section class="container">
    <h1>定时任务管理</h1>
    <div class="line"></div>
    <div class="cl pd-5 bk-gray mt-10">
        <form id="listForm" action="${path}/scheduleJob/list.json"> <span class="l"><a
                href="javascript:;"
                onclick="layer_show('添加','${path}/scheduleJob/add','','500')"
                class="btn btn-primary radius"><i class="Hui-iconfont">&#xe600;</i>
            添加</a></span></form>
    </div>
    <table class="table table-border table-bordered table-striped mt-20">
        <thead>
        <tr>
            <th class="col2">任务组</th>
            <th class="col1">任务名</th>
            <th class="col3">cron表达式</th>
            <th class="col3">状态</th>
            <th class="col3">任务执行类名称</th>
            <th class="col3">任务执行类方法</th>
            <th class="col3">描述</th>
            <th class="col3">操作</th>
        </tr>
        </thead>
        <tbody id="view">
        </tbody>
    </table>
    <div id="page"></div>
</section>
<script id="demo" type="text/html">
    {{# for(var i = 0, len = d.list.length; i < len; i++){ }}
    {{# var l = d.list[i]}}
    <tr class="text-c">
        <td>{{l.jobGroup}}</td>
        <td>{{l.jobName}}</td>
        <td>{{l.cronExpression}}</td>
        <td>{{# var a=l.status; if(l.status=='PAUSED'){a='暂停';}else if(l.status=='NORMAL'){a='正常';} }}
            {{a }}
        </td>
        </td>

        <td>{{l.className}}</td>
        <td>{{l.methodName}}</td>
        <td>{{l.description}}</td>
        <td class="parentTd" jobGroup="{{l.jobGroup}}" jobName="{{l.jobName}}">
            <a class="paused btn  size-MINI btn-secondary radius" href="javascript:void(0); ">暂停</a>
            <a class="resume btn btn-danger size-MINI radius" href="javascript:void(0); ">恢复</a>
            <a class="startNow btn btn-danger size-MINI radius" href="javascript:void(0); ">立即执行</a>
            <a class="delete btn btn-warning size-MINI radius" href="javascript:void(0); ">删除</a></td>
    </tr>
    {{# } }}
</script>
<jsp:include page="../../common/foot.jsp"></jsp:include>
<script>
    $(function () {
        initPage("listForm", "demo", "view", "page");
    })

    $(document).on("click", ".paused", function () {
        doSomething($(this), "${path}/scheduleJob/stopJob");
    });
    $(document).on("click", ".resume", function () {
        doSomething($(this), "${path}/scheduleJob/resume");
    });
    $(document).on("click", ".delete", function () {
        doSomething($(this), "${path}/scheduleJob/delete");
    })
    $(document).on("click", ".startNow", function () {
        doSomething($(this), "${path}/scheduleJob/startNow");
    })
    function doSomething(thisObj, url) {
        var jobGroup = thisObj.parent(".parentTd").attr("jobGroup");
        var jobName = thisObj.parent(".parentTd").attr("jobName");
        $.post(url, {"jobGroup": jobGroup, "jobName": jobName}, function (data) {
            data = eval("(" + data + ")");
            layer.msg(data.msg, {icon: 6, time: 1000});
            setTimeout(function () {
                initPage("listForm", "demo", "view", "page");
            }, 1000);
        });
    }
</script>
</body>
</html>