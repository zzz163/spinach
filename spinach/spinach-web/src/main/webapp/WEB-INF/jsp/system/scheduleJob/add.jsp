﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<c:set var="path" value="${pageContext.request.contextPath}" />
<!DOCTYPE HTML>
<html>
<jsp:include page="../../common/head.jsp"></jsp:include>
<link rel="stylesheet" type="text/css" href="${path}/static/css/H-ui.css"/>
<title>添加任务</title>
</head>
<body>
<section class="container">
    <form action="${path}/scheduleJob/save.json" method="post" class="form form-horizontal responsive" id="demoform">
        <div class="row cl">
            <label class="form-label col-xs-3">任务组：</label>
            <div class="formControls col-xs-5">
                <input type="text" class="input-text" name="jobGroup" datatype="*" nullmsg="请输入任务组！">
            </div>
            <div class="col-xs-3"></div>
        </div>
        <div class="row cl">
            <label class="form-label col-xs-3">任务名：</label>
            <div class="formControls col-xs-5">
                <input type="text" class="input-text" name="jobName" datatype="*" nullmsg="请输入任务名！">
            </div>
            <div class="col-xs-3"></div>
        </div>
        <div class="row cl">
            <label class="form-label col-xs-3">cron表达式：</label>
            <div class="formControls col-xs-5">
                <input type="text" class="input-text" name="cronExpression" datatype="*" nullmsg="请输入cron表达式！">
            </div>
            <div class="col-xs-3"></div>
        </div>
        <div class="row cl">
            <label class="form-label col-xs-3">任务执行类名称：</label>
            <div class="formControls col-xs-5">
                <input type="text" class="input-text" name="className" datatype="*" nullmsg="请输入任务执行类名称！">
            </div>
            <div class="col-xs-3"></div>
        </div>
        <div class="row cl">
            <label class="form-label col-xs-3">任务执行类方法：</label>
            <div class="formControls col-xs-5">
                <input type="text" class="input-text" name="methodName" datatype="*" nullmsg="请输入任务执行类方法！">
            </div>
            <div class="col-xs-3"></div>
        </div>

        <div class="row cl">
            <label class="form-label col-xs-3">备注：</label>
            <div class="formControls col-xs-5">
                <textarea name="description" cols="" rows="" class="textarea" placeholder="说点什么..."
                          onKeyUp="textarealength(this,500)"></textarea>
                <p class="textarea-numberbar"><em class="textarea-length">0</em>/500</p>
            </div>
            <div class="col-xs-3"></div>
        </div>
        <div class="row cl">
            <div class="col-9 col-offset-3">
                <input class="btn btn-success radius" type="submit"
                       value="&nbsp;&nbsp;提交&nbsp;&nbsp;"> &nbsp; &nbsp; &nbsp; &nbsp;<input
                    class="btn btn-default radius" type="button"
                    value="&nbsp;&nbsp;返回&nbsp;&nbsp;">
            </div>
        </div>
    </form>
</section>
<jsp:include page="../../common/foot.jsp"></jsp:include>
<script type="text/javascript">
    $(function () {
        $("#demoform").validFrom(function (data) {
            if (data.status == 0) {
                var index = parent.layer.getFrameIndex(window.name);
                parent.initPage("listForm", "demo", "view", "page");
                parent.layer.close(index);
                layer.alert(data.msg, {icon: 6});
            }else{
               layer.alert(data.msg, {icon: 5});
            }
        });
    });

</script>
</body>
</html>